import React, { Component } from 'react';
import { connect } from 'react-redux';
import './Bulb.css';

class Bulb extends Component {

    render() {
        return (
            <div className={ this.props.light? "bulb on_bulb" : "bulb off_bulb" }>
                { this.props.light? "ON" : "OFF" }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return { light: state.light.on};
};

export default connect(mapStateToProps,null) (Bulb);