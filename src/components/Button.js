import React, { Component } from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import {flipSwitch} from '../actions/ActionCreator'

class Button extends Component {

    render() {
        return (
            <div>
                <button 
                    onClick={() => this.props.flipSwitch(!this.props.light)} 
                    className={this.props.light? "btn btn-success": "btn btn-danger"}>
                    {this.props.light? "ON": "OFF"}
                </button>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        flipSwitch
    }, dispatch)
};

const mapStateToProps = (state) => {
    return { light: state.light.on};
};

export default connect(mapStateToProps,mapDispatchToProps) (Button);