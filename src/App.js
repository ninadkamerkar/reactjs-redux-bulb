import React, { Component } from 'react';
import './App.css';

import Bulb from './components/Bulb';
import Button from './components/Button';

class App extends Component {
  render() {
    return (
      <div className="container bulb-box" >
        <div className="row">
          <div className="col-6">
            <Button/>
          </div>
          <div className="col-6">
            <Bulb/>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
