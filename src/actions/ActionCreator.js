import {FLIP_SWITCH} from './ActionTypes';

export const flipSwitch = (true_false) => ({
    type: FLIP_SWITCH,
    flip: true_false
})