import {FLIP_SWITCH} from '../actions/ActionTypes';

const INITIAL_STATE = {on: false}

const SwitchReducer = (state = INITIAL_STATE, action) => {
    switch (action.type)  {
        case FLIP_SWITCH: 
            return {on: !state.on}
        default:
            return state
    }
}

export default SwitchReducer;